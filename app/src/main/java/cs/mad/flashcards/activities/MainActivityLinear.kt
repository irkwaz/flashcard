package cs.mad.flashcards.activities

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapterLinear
import cs.mad.flashcards.adapters.FlashcardSetAdapterGrid
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

class MainActivityLinear : AppCompatActivity() {
    private var sets = Flashcard.getHardcodedFlashcards().toMutableList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_linear)


//ignore this action bar stuff me trying to put the add button on the action bar
        val actionBar = supportActionBar
        actionBar!!.title = "Flashcards"
        actionBar.setDisplayHomeAsUpEnabled(true)
        LinearLayoutManager(this)

        val itemFlashAdapter = FlashcardAdapterLinear(sets)

//        Linear view
        val recycler = findViewById<RecyclerView>(R.id.recyclerLinearView)
        recycler.adapter = itemFlashAdapter

        val textView: TextView = findViewById(R.id.addBtn)
        textView.setOnClickListener {addNewTerm()}
    }
    private fun addNewTerm() {
        sets.add(Flashcard("Term", "Definition"))
        val itemFlashAdatpater = FlashcardAdapterLinear(sets)
        val recyclerLin = findViewById<RecyclerView>(R.id.recyclerLinearView)
        recyclerLin.adapter = itemFlashAdatpater

    }
}