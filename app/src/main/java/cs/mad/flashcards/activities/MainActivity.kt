package cs.mad.flashcards.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapterGrid
import cs.mad.flashcards.entities.FlashcardSet

class MainActivity : AppCompatActivity() {

    private var sets = FlashcardSet.flashcardSets().toMutableList()
    private var adapter = FlashcardSetAdapterGrid(sets)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_grid)


        val recyclergrid = findViewById<RecyclerView>(R.id.recycler_grid)
        recyclergrid.adapter = FlashcardSetAdapterGrid(sets)
        recyclergrid.layoutManager = GridLayoutManager(this, 2)
        recyclergrid.adapter = adapter

        //add button listener
        val textView: TextView = findViewById(R.id.addBtn1)
        textView.setOnClickListener { addNewSets() }
    }
    private fun addNewSets() {
        sets.add(FlashcardSet("New Flashcard Set"))
        val newItemAdapter = FlashcardSetAdapterGrid(sets)
        val recyclergrid = findViewById<RecyclerView>(R.id.recycler_grid)
        recyclergrid.adapter = newItemAdapter

    }
}