package cs.mad.flashcards.adapters

import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard

class FlashcardAdapterLinear(input: List<Flashcard>):
    RecyclerView.Adapter<FlashcardAdapterLinear.ViewHolder>() {

    private val flashcard = input.toMutableList()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textview: TextView = view.findViewById(R.id.myFlashcardData)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard_linear, viewGroup, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textview.text = flashcard[position].term
        holder.textview.text = flashcard[position].definition
    }

    override fun getItemCount(): Int {
        return flashcard.size
    }
}