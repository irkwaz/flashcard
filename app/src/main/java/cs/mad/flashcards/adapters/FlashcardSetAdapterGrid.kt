package cs.mad.flashcards.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import cs.mad.flashcards.activities.MainActivityLinear
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.FlashcardSet

class FlashcardSetAdapterGrid(val input: MutableList<FlashcardSet>):
    RecyclerView.Adapter<FlashcardSetAdapterGrid.ViewHolder>() {

    private val flashcardset = input

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textview = view.findViewById<TextView>(R.id.flashcardText)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard_set_grid,viewGroup, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textview.text = flashcardset[position].title
        holder.itemView.setOnClickListener{holder.itemView.context.startActivity(Intent(
            holder.itemView.context, MainActivityLinear::class.java))
        }
    }

    override fun getItemCount(): Int {
        return flashcardset.size
    }
}