package cs.mad.flashcards.entities

data class FlashcardSet(val title: String) {
    companion object {
        fun flashcardSets(): List<FlashcardSet> {
            val hardcoded = mutableListOf<FlashcardSet>()
            for (i in 1..10) {
                hardcoded.add(FlashcardSet("Flashcard Set $i"))
            }
            return hardcoded
        }
    }
}

